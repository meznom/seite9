<!-- 
.. title: Hochzeit Einladung
.. slug: hochzeiteinladung
.. date: 2014/03/26 11:50:13
.. tags: 
.. link: 
.. description: 
.. type: text
.. template: wedding.tmpl
.. hidetitle: True
-->

<div id="invitation-carousel" class="carousel slide" data-ride="carousel">
  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="/img/2014/Hochzeitseinladung_Burkhard_und_Esly.1.png" class="img-responsive img-rounded center-block" title="Hochzeitseinladung Vorderseite" alt="Hochzeitseinladung Vorderseite" />
    </div>
    <div class="item">
      <img src="/img/2014/Hochzeitseinladung_Burkhard_und_Esly.2.png" class="img-responsive img-rounded center-block" title="Hochzeitseinladung Rueckseite" alt="Hochzeitseinladung Rueckseite" />
    </div>
  </div>
  <!-- Indicators -->
  <ol class="carousel-indicators invitation-carousel-indicators">
    <li data-target="#invitation-carousel" data-slide-to="0" class="active"></li>
    <li data-target="#invitation-carousel" data-slide-to="1"></li>
  </ol>

</div>


<div class="page-header">
<ul class="nav nav-pills navbar-right">
  <li><a href="#">Deutsch</a></li>
  <li><a href="../../es/stories/hochzeiteinladung.html">Español</a></li>
</ul>
<h2>Ansprache</h2>
</div>
<p class="lead">Liebe&nbsp;Gäste,</p>
<p class="lead">wir laden Euch ganz herzlich zu unserem Hochzeitsfest am 26. Juli 2014 im
Theatercafé in Arnstadt ein. Bitte gebt uns bis Ende April
<a href="mailto:Burkhard%20Ritter%20&lt;burkhard@ualberta.ca&gt;">Bescheid</a>, ob Ihr kommen könnt.
In den folgenden Wochen und Monaten werden wir diese Webseite mit den neusten
Nachrichten und tollsten Entwicklungen rund um die große Festlichkeit&nbsp;bestücken.</p>
<p class="lead">Wir freuen uns auf Euer Kommen,<br>
Esly und&nbsp;Burkhard</p>



<h2 class="page-header">Anfahrt</h2>
<div id="map-arnstadt" style="height:400px;"></div>



<h2 class="page-header">Unterkunft</h2>
<div class="row">
  <div class="col-md-6">
    <p>Hotel Krone am Bahnhof<br>
    Am Bahnhof 8<br>
    99310 Arnstadt<br>
    Telefon: 03628 77060<br>
    Email: <a href="mailto:krone-2000@t-online.de">krone-2000@t-online.de</a><br>
    Webseite: <a href="http://www.krone-2000.de">www.krone-2000.de</a></p>
  </div>
  <div class="col-md-6">
    <p>Unter dem Kennwort &bdquo;Hochzeit Esly und Burkhard&ldquo; sind Zimmer am 26.7.2014 zu
    folgenden Preisen reserviert:<br>
    Einzelzimmer: 55€<br>
    Doppelzimmer: 66€<br>
    Dreibettzimmer: 75€<br>
    Vierbettzimmer:&nbsp;90€  </p>
  </div>
</div>



<h2 class="page-header">Stand der Dinge</h2>
27\. März 2014. Die Hochzeitswebseite steht endlich im Netz.  
3\. April 2014. Versíon español.
 

<!-- Load Google Maps -->
<script type="text/javascript">
function initializeGoogleMaps() {
  var mapOptions = {
    zoom: 15,
    center: {lat: 50.837512, lng: 10.953016}
  };

  var mapElement = document.getElementById('map-arnstadt');
  var map = new google.maps.Map(mapElement, mapOptions);
  var infowindow = new google.maps.InfoWindow();

  // In Javascript scope is per function body...
  var openWindow = function(m,p) {
    return function() {
      infowindow.setContent(p['title']);
      infowindow.open(map,m);
    };
  };

  // markers
  var ms = [];
  var ps = [{lat: 50.836512, lng: 10.953016, title: 'Theatercafé Arnstadt'},
            {lat: 50.841309, lng: 10.947866, title: 'Hotel Krone'},
            {lat: 50.833622, lng: 10.946005, title: 'Familie Ritter'},
            {lat: 50.838324, lng: 10.951493, title: 'Zufahrt Theatercafé'}];
  for (var i in ps) {
    var p = ps[i];
    var m = new google.maps.Marker({
      position: p,
      map: map,
      title: p['title']
    });
    google.maps.event.addListener(m, 'click', openWindow(m,p));
    ms.push(m);
  }
}

function loadScript() {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'http://maps.googleapis.com/maps/api/js?' + 
               'key=AIzaSyCwPcG-WwxnzPY3itsNIGjsbOqVb2fK0u4&' +
               'v=3.exp&' + 
               'sensor=false&' +
               'callback=initializeGoogleMaps';
  document.body.appendChild(script);
}

window.onload = loadScript;
</script>
