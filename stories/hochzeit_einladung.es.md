<!-- 
.. title: Hochzeit Einladung
.. slug: hochzeiteinladung
.. date: 2014/03/26 11:50:13
.. tags: 
.. link: 
.. description: 
.. type: text
.. template: wedding.tmpl
.. hidetitle: True
-->

<div id="invitation-carousel" class="carousel slide" data-ride="carousel">
  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="/img/2014/Hochzeitseinladung_Burkhard_und_Esly.1.png" class="img-responsive img-rounded center-block" title="Hochzeitseinladung Vorderseite" alt="Hochzeitseinladung Vorderseite" />
    </div>
    <div class="item">
      <img src="/img/2014/Hochzeitseinladung_Burkhard_und_Esly.2.png" class="img-responsive img-rounded center-block" title="Hochzeitseinladung Rueckseite" alt="Hochzeitseinladung Rueckseite" />
    </div>
  </div>
  <!-- Indicators -->
  <ol class="carousel-indicators invitation-carousel-indicators">
    <li data-target="#invitation-carousel" data-slide-to="0" class="active"></li>
    <li data-target="#invitation-carousel" data-slide-to="1"></li>
  </ol>

</div>

<div class="page-header">
<ul class="nav nav-pills navbar-right">
  <li><a href="../../de/stories/hochzeiteinladung.html">Deutsch</a></li>
  <li><a href="#">Español</a></li>
</ul>
<h2>Discurso</h2>
</div>
<p class="lead">¡Queridos invitados!</p>
<p class="lead">Estan cordialmente invitados a la celebracion de nuestra boda,
el día 26 de julio del 2014 en el Theatercafé en Arnstadt, Alemania.
Agradecemos que por favor <a href="mailto:esly.alvarez@gmail.com">confirmen
asistencia</a> para antes del primero de mayo. En las siguientes semanas
tendrémos aquí mas novedades e informacíon sobre este acontecimiento tan
especial.</p>
<p class="lead">Esperamos que puedan acompañarnos,<br>
Esly y&nbsp;Burkhard</p>



<h2 class="page-header">Direccíon del evento</h2>
<div id="map-arnstadt" style="height:400px;"></div>



<h2 class="page-header">Hospedaje</h2>
<div class="row">
  <div class="col-md-6">
    <p>Hotel Krone am Bahnhof<br>
    Am Bahnhof 8<br>
    99310 Arnstadt<br>
    Telefóno: +49 3628 77060<br>
    Email: <a href="mailto:krone-2000@t-online.de">krone-2000@t-online.de</a><br>
    Pagína web: <a href="http://www.krone-2000.de">www.krone-2000.de</a></p>
  </div>
  <div class="col-md-6">
    <p>Contactarnos para ayuda con las reservaciones. Precios especiales al
    reservar para la boda de Esly y Burkhard el 26 de julio del 2014: <br>
    Habitacíon sencilla: 55€<br>
    Habitacíon doble: 66€<br>
    Habitacíon triple: 75€<br>
    Habitacíon quadruple:&nbsp;90€  </p>
  </div>
</div>



<h2 class="page-header">Estado actual</h2>
27\. Marzo 2014. Pagína web esta activa.  
3\. Abril 2014. Versíon español.
 

<!-- Load Google Maps -->
<script type="text/javascript">
function initializeGoogleMaps() {
  var mapOptions = {
    zoom: 15,
    center: {lat: 50.837512, lng: 10.953016}
  };

  var mapElement = document.getElementById('map-arnstadt');
  var map = new google.maps.Map(mapElement, mapOptions);
  var infowindow = new google.maps.InfoWindow();

  // In Javascript scope is per function body...
  var openWindow = function(m,p) {
    return function() {
      infowindow.setContent(p['title']);
      infowindow.open(map,m);
    };
  };

  // markers
  var ms = [];
  var ps = [{lat: 50.836512, lng: 10.953016, title: 'Theatercafé Arnstadt'},
            {lat: 50.841309, lng: 10.947866, title: 'Hotel Krone'},
            {lat: 50.833622, lng: 10.946005, title: 'Familie Ritter'},
            {lat: 50.838324, lng: 10.951493, title: 'Zufahrt Theatercafé'}];
  for (var i in ps) {
    var p = ps[i];
    var m = new google.maps.Marker({
      position: p,
      map: map,
      title: p['title']
    });
    google.maps.event.addListener(m, 'click', openWindow(m,p));
    ms.push(m);
  }
}

function loadScript() {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'http://maps.googleapis.com/maps/api/js?' + 
               'key=AIzaSyCwPcG-WwxnzPY3itsNIGjsbOqVb2fK0u4&' +
               'v=3.exp&' + 
               'sensor=false&' +
               'callback=initializeGoogleMaps';
  document.body.appendChild(script);
}

window.onload = loadScript;
</script>
