<!-- 
.. title: Hochzeit
.. slug: hochzeit
.. date: 2014/09/14 13:40:00
.. tags: 
.. link: 
.. description: 
.. type: text
.. template: wedding.tmpl
.. hidetitle: True
-->

<img alt="Hochzeitsdankeskarte" src="../../img/2014/Hochzeitsdankeskarte_Burkhard_und_Esly.jpg" class="wedding-image img-circle center-block">

<div class="page-header">
  <ul class="nav nav-pills navbar-right">
    <li><a href="#">Deutsch</a></li>
    <li><a href="../../es/stories/hochzeit.html">Español</a></li>
  </ul>
  <h2>Danke</h2>
</div>

<p class="lead">Liebe Familie, liebe Freunde,</p>
<p class="lead">
wir bedanken uns für die Glückwünsche, Geschenke und vor allem, dass wir unsere
Hochzeit zusammen mit Euch feiern konnten. Wir hatten eine tolle Zeit, in
Deutschland, in Mexiko und in den Wochen danach. Und wie nebenbei sind wir nun
vor der beschaulichen Kulisse verregneter deutscher Keinstädte beschwingt im
Eheleben unterwegs.
</p>
<p class="lead">Vielen Dank und bis bald,<br>
Esly und Burkhard
</p>

<h2 class="page-header">Fotos</h2>

[Bilder von der Hochzeit in Deutschland](../../galleries/hochzeitdeutschland1/), vom Fotograf Delf Zeh.  
[Bilder von der Hochzeit in Deutschland](../../galleries/hochzeitdeutschland2/), von Thomas und Sabine.  
[Bilder von der Hochzeit in Mexiko](../../galleries/hochzeitmexiko1/), von Miguel Alvarez.

---

September 2014. Zur ursprüngliche [Hochzeitseinladungsseite](../../de/stories/hochzeiteinladung.html).
