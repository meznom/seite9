<!-- 
.. title: Hochzeit
.. slug: hochzeit
.. date: 2014/03/26 11:50:13
.. tags: 
.. link: 
.. description: 
.. type: text
.. template: wedding.tmpl
.. hidetitle: True
-->

<img alt="Hochzeitsdankeskarte" src="../../img/2014/Hochzeitsdankeskarte_Burkhard_und_Esly.jpg" class="wedding-image img-circle center-block">

<div class="page-header">
  <ul class="nav nav-pills navbar-right">
    <li><a href="../../de/stories/hochzeit.html">Deutsch</a></li>
    <li><a href="#">Español</a></li>
  </ul>
  <h2>Gracias</h2>
</div>

<p class="lead">¡Querida familia y amigos!</p>
<p class="lead">
Queremos agradecerles por los obsequios, los buenos deseos y sobre todo su gráta compañia 
en los que fueron unos de los dias mas importantes para ambos. De todo este viaje y tiempo
que hemos pasado ambos, por mucho esta a sido una de las mejores experiencias. Gracias por
todo el apoyo que nos brindaron tanto en Mexico como Alemania.
</p>
<p class="lead">
Burkhard y Esly
</p>

<h2 class="page-header">Fotografías</h2>

[Imagenes de la boda en Alemania](../../galleries/hochzeitdeutschland1/), del fotografo Delf Zeh.  
[Imagenes de la boda en Alemania](../../galleries/hochzeitdeutschland2/), de Thomas y Sabine.  
[Imagenes de la boda en Mexico](../../galleries/hochzeitmexiko1/), de Miguel Alvarez.

---

Septiembre del 2014. Para la [pagina de la invitación original](../../es/stories/hochzeiteinladung.html).
