<!-- 
.. title: About
.. slug: about
.. date: 2014/02/09 17:19:48
.. tags: 
.. link: 
.. description: 
.. type: text
-->

<img src="/img/scrambling.png" class="img-responsive img-rounded pull-right" style="margin-left: 10px;" title="Me" alt="Me" />
This is Burkhard Ritter on the web. I am a graduate student in the [Department
of Physics][1] at the [University of Alberta][2], working with [Kevin
Beach][3]. I have a few small projects on [Bitbucket][4] and [GitHub][5]. You
can contact me via [email](mailto:Burkhard Ritter <burkhard@seite9.de>).

About this website: The design is based on Bootstrap's [Blog example][7] and
was originally inspired by the [prsnl10][8] Dokuwiki template. The site is
generated with [Nikola][9] and hosted by [Domainfactory][10].

[1]: http://www.physics.ualberta.ca
[2]: http://www.ualberta.ca
[3]: http://www.ualberta.ca/~kbeach
[4]: https://bitbucket.org/meznom
[5]: https://github.com/meznom
[7]: http://getbootstrap.com/getting-started/#examples
[8]: http://andreas-haerter.com/projects/dokuwiki-template-prsnl10
[9]: http://getnikola.com/
[10]: http://www.df.eu/
