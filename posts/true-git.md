<!-- 
.. title: True Git
.. slug: true-git
.. date: 2013/08/01 00:00:00
.. tags: 
.. link: 
.. description: 
.. type: text
-->

The [Graduate Physics Student Association][GPSA] here at the University of
Alberta has been organizing a Graduate Student Seminar Series for a couple of
weeks now and I would say it's one of the more successful and well attended GPSA
initiatives, with a turnout of about 30 to 40 people every Thursday. (Note that
I am a member of the GPSA council and hence likely not the most objective
observer.)

Earlier today I had the pleasure of giving a presentation at this seminar. I
opted for an introductory talk on [Git][] and version control ([slides][],
[pdf][]).  About a third of the audience already knew how to use Git and was
probably bored to death. I think I lost half of the audience when I pulled up a
command line prompt. That leaves one sixth who really enjoyed the talk, which
really is not a bad result. Two people actually stayed for my brief hands-on
session after the talk and so I was quite happy.

This is the second presentation I prepared with [reveal.js][] and while it's far
from perfect, it's nice to get simple presentations done quickly (e.g. in half a
night). I am not very happy with some of the default formatting with reveal.js,
but this is something that probably could be fixed easily if I would only take
the time to tinker with the CSS theme. More importantly, reveal.js is just not
good for not-text-heavy, graphics and maths rich presentations&mdash;and in my
opinion presentations should contain as little text as possible. After all the
audience is supposed to listen to you talking and not read lengthy texts on the
slides.  For one of these more graphics-heavy presentations I used [Inkscape][]
and its build-in presentation plugin recently. Again, this approach is far
from perfect. The usage of the plugin feels a bit clumsy and slides with lots of
text (e.g. code with syntax highlighting) are probably not something it excels
at.  Still, overall I think I prefer to craft my presentations graphically with
Inkscape and in the end I was really happy with the results. However, there's no
denying that it takes much, much more time.

Finally, the obvious question might be why am I not using one of the "standard
programs", like PowerPoint or Keynote. The former does not run on any of my
computers, the latter only on my iMac, but not on my Ubuntu Laptop. Still, I
might be inclined to try it. I ditched Open/Libre Office long ago, after it
managed to disappear half of my slides half an hour before my presentation in an
impressive magic trick tour de force. I've used Latex Beamer for a couple of
years, but its blue-yellow charm wears off rather quickly.

[GPSA]: http://gpsa.physics.ualberta.ca/
[Git]: http://git-scm.com/
[slides]: http://seite9.de/o/presentations/2013-08-01/#/
[pdf]: http://seite9.de/o/presentations/2013-08-01_GradSeminarGitPresentation.pdf
[reveal.js]: https://github.com/hakimel/reveal.js
[Inkscape]: http://inkscape.org/
