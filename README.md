# Seite9

My personal website. Uses [Nikola][10] as the static site generator. The design
is based on Bootstrap's [Blog example][15] and was originally inspired by
Andreas Haerter's [Dokuwiki][20] template [prsnl10][30].

Burkhard, February 2014

## Random notes

### Resizing images

    $ for i in *; do mv $i `basename $i .png`.orig.png; done
    # Only resize if they are actually wider than 600px.
    $ for i in *.orig.png; do convert $i -resize "600>" `basename $i .orig.png`.png; done

### Git

To push to two git remote repositories at the same time, modify `.git/config`
to contain something like

    [remote "origin"]
        fetch = +refs/heads/*:refs/remotes/origin/*
        url = git@bitbucket.org:meznom/seite9.git
        url = /Users/burkhard/o/src/repos/seite9.git

I read this on a [blog][50].

### FTP

[Yafc][60] is a nice ftp client that supports sftp. [Lftp][70] is my old
favourite and it still seems to be the best. For keeping files in sync its
reverse mirror feature (`mirror -R`) is invaluable. For lftp not to ask for a
password and just use the ssh key for authentication, we can use

    lftp -u [username],xx sftp://[host]

that is, just provide a bogus password `xx`.

### Google

* [Author information in search results](https://support.google.com/webmasters/answer/1408986?expand=option2)
* [Structured data testing tool](http://www.google.com/webmasters/tools/richsnippets)

[10]: http://getnikola.com/ 
[15]: http://getbootstrap.com/getting-started/#examples
[20]: http://www.dokuwiki.org/dokuwiki
[30]: http://andreas-haerter.com/projects/dokuwiki-template-prsnl10
[31]: https://github.com/mojombo/jekyll/issues/422
[32]: http://stackoverflow.com/questions/7801197/syntax-highlighting-with-pygments-is-failing-via-liquid-templates-string-error
[40]: https://github.com/blog/832-rolling-out-the-redcarpet
[50]: http://blog.rodhowarth.com/2011/11/general-devhow-to-push-to-two-git.html
[60]: http://yafc.sourceforge.net/
[70]: http://lftp.yar.ru/
